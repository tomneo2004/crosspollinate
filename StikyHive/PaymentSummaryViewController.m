//
//  PaymentSummaryViewController.m
//  StikyHive
//
//  Created by User on 27/11/15.
//  Copyright © 2015 Stiky Hive. All rights reserved.
//

#import "PaymentSummaryViewController.h"
#import "WebDataInterface.h"
#import "LocalDataInterface.h"
#import "PostRequestManager.h"
#import "UIView+RNActivityView.h"

@interface PaymentSummaryViewController ()

@property (nonatomic, strong, readwrite) PayPalConfiguration *payPalConfiguration;

@end

@implementation PaymentSummaryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [PayPalMobile preconnectWithEnvironment:PayPalEnvironmentSandbox];
/*
#ifdef DEBUG
    
    [PayPalMobile preconnectWithEnvironment:PayPalEnvironmentSandbox];
    
#else
    
    [PayPalMobile preconnectWithEnvironment:PayPalEnvironmentProduction];
    
#endif
*/
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)ProceedPayment:(id)sender{
    
    // Create a PayPalPayment
    PayPalPayment *payment = [[PayPalPayment alloc] init];
    
    // Amount, currency, and description
    payment.amount = [[NSDecimalNumber alloc] initWithString:@"5.00"];
    payment.currencyCode = @"SGD";
    payment.shortDescription = @"Post a request";
    
    // Use the intent property to indicate that this is a "sale" payment,
    // meaning combined Authorization + Capture.
    // To perform Authorization only, and defer Capture to your server,
    // use PayPalPaymentIntentAuthorize.
    // To place an Order, and defer both Authorization and Capture to
    // your server, use PayPalPaymentIntentOrder.
    // (PayPalPaymentIntentOrder is valid only for PayPal payments, not credit card payments.)
    payment.intent = PayPalPaymentIntentSale;
    
    // If your app collects Shipping Address information from the customer,
    // or already stores that information on your server, you may provide it here.
    //payment.shippingAddress = address; // a previously-created PayPalShippingAddress object
    
    // Several other optional fields that you can set here are documented in PayPalPayment.h,
    // including paymentDetails, items, invoiceNumber, custom, softDescriptor, etc.
    
    // Check whether payment is processable.
    if (!payment.processable) {
        // If, for example, the amount was negative or the shortDescription was empty, then
        // this payment would not be processable. You would want to handle that here.
    }
    
    // Create a PayPalPaymentViewController.
    PayPalPaymentViewController *paymentViewController;
    paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:payment
                                                                   configuration:self.payPalConfiguration
                                                                        delegate:self];
    
    // Present the PayPalPaymentViewController.
    [self presentViewController:paymentViewController animated:YES completion:nil];
}

#pragma mark - PayPalPaymentDelegate
- (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController didCompletePayment:(PayPalPayment *)completedPayment {

    [self uploadDataToServer];
}

- (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController {
    // The payment was canceled; dismiss the PayPalPaymentViewController.
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - internal
- (void)uploadDataToServer{
    
    PostRequestManager *mgr = [PostRequestManager sharedPostRequestManager];
    
    [self.view showActivityViewWithLabel:@"Uploading..." detailLabel:@"Uploading your request"];
    
    //upload urgent request to server
    [WebDataInterface insertUrgentRequest:[LocalDataInterface retrieveStkid] title:mgr.title desc:mgr.postDesc completion:^(NSObject *obj, NSError *error){
    
        dispatch_async(dispatch_get_main_queue(), ^{
        
            if(error != nil){
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Unable to upload data to server" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                [alert show];
                
                [self.view hideActivityView];
                
                [self.navigationController popToRootViewControllerAnimated:YES];
                
                return ;
            }
            
            NSDictionary *dic = [(NSDictionary *)obj objectForKey:@"result"];
            
            //upload photo to server
            [WebDataInterface fileRequestUpload:mgr.attachmentImage stikyid:[LocalDataInterface retrieveStkid] cpid:[[dic objectForKey:@"cpId"] integerValue]];
            
            
            
            //hide activity
            [self.view hideActivityView];
            
            // Dismiss the PayPalPaymentViewController.
            [self dismissViewControllerAnimated:YES completion:^{
                
                //push payment successful controller
                PaymentSuccessfulViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"PaymentSuccessfulViewController"];
                controller.delegate = self;
                
                [self.navigationController presentViewController:controller animated:YES completion:nil];
            }];
        });
    }];
    
    
}

#pragma mark - PaymentSuccessful delegate
- (void)onBackToCrossPollinateTap:(PaymentSuccessfulViewController *)controller{
    
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
    
        [self.navigationController popToRootViewControllerAnimated:YES];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
