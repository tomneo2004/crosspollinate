//
//  LogoutViewController.h
//  StikyHive
//
//  Created by Koh Quee Boon on 31/8/15.
//  Copyright (c) 2015 Stiky Hive. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LogoutViewController : UIViewController
- (IBAction)yesButtonPressed:(id)sender;
- (IBAction)noButtonPressed:(id)sender;

@end
