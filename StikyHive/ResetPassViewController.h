//
//  ResetPassViewController.h
//  StikyHive
//
//  Created by Koh Quee Boon on 10/9/15.
//  Copyright (c) 2015 Stiky Hive. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResetPassViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *getPassLabel;

- (IBAction)backLoginButtonPressed:(id)sender;

@end
