//
//  MyProfileViewController.h
//  StikyHive
//
//  Created by THV1WP15S on 13/11/15.
//  Copyright (c) 2015 Stiky Hive. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyProfileViewController : UIViewController <UIScrollViewDelegate,UIWebViewDelegate>



@property (strong, nonatomic) IBOutlet UIScrollView *contentScrollView;

@end
