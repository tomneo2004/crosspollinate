//
//  DistanceTitle.h
//  StikyHive
//
//  Created by User on 24/11/15.
//  Copyright © 2015 Stiky Hive. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DistanceTitle : UIView

@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;

@end
