//
//  PhotoViewController.h
//  StikyHive
//
//  Created by Koh Quee Boon on 2/6/15.
//  Copyright (c) 2015 Stiky Hive. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoViewController : UIViewController

+ (void)setPhotoPath:(NSString *) photoPath;
+ (void)setImage:(UIImage *) image;

@end
