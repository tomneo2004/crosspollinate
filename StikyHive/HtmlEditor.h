//
//  HtmlTextView.h
//  StikyHive
//
//  Created by Koh Quee Boon on 20/7/15.
//  Copyright (c) 2015 Stiky Hive. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZSSRichTextEditor.h"

@interface HtmlEditor : ZSSRichTextEditor

@end
