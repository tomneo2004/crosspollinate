//
//  RequestPostTableViewController.h
//  StikyHive
//
//  Created by User on 17/11/15.
//  Copyright © 2015 Stiky Hive. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Request.h"

@interface RequestPostTableViewController : UITableViewController

@property (weak, nonatomic) Request *request;

@end
