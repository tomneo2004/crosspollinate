//
//  PostCommViewController.h
//  StikyHive
//
//  Created by THV1WP15S on 9/10/15.
//  Copyright (c) 2015 Stiky Hive. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PostCommViewController : UIViewController <UITextViewDelegate>

- (void)setSkillID:(NSString *)skillID;

@end
