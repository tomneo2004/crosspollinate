//
//  BuyerPageViewController.h
//  StikyHive
//
//  Created by THV1WP15S on 21/9/15.
//  Copyright (c) 2015 Stiky Hive. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BuyerPageViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@end
